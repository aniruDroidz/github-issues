package com.anirudroidz.githubissues

import android.os.Bundle
import android.util.Log
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.anirudroidz.githubissues.data.model.Error
import com.anirudroidz.githubissues.data.model.issue.Issue
import com.anirudroidz.githubissues.domain.Callback
import com.anirudroidz.githubissues.domain.interactor.GitHubIssueCommentsInteractor
import com.anirudroidz.githubissues.domain.interactor.GitHubIssuesInteractor
import com.anirudroidz.githubissues.domain.local.AppDatabase
import kotlinx.coroutines.launch

class MainViewModel(
    private val issuesInteractor: GitHubIssuesInteractor,
    private val issueCommentsInteractor: GitHubIssueCommentsInteractor,
    private val db: AppDatabase,
    val observables: MainObservables
) : ViewModel() {

    fun initialiseMainFragment() {
        viewModelScope.launch {
            issuesInteractor.execute(
                object : Callback<List<Issue>> {
                    override fun onResponse(response: List<Issue>) {
                        observables.issuesObservable.postValue(response)
                    }

                    override fun onError(errorCode: String, errorMessage: String) {
                        observables.errorObservable.postValue(Error(errorCode, errorMessage))
                    }
                },
                db,
                viewModelScope
            )
        }
    }

    fun initialiseCommentsFragment(commentsUrl: String) {
        viewModelScope.launch {
            issueCommentsInteractor.execute(
                commentsUrl,
                object :
                    Callback<List<com.anirudroidz.githubissues.data.model.comment.Comment>> {
                    override fun onResponse(response: List<com.anirudroidz.githubissues.data.model.comment.Comment>) {
                        observables.commentsObservable.postValue(response)
                    }

                    override fun onError(errorCode: String, errorMessage: String) {
                        observables.errorObservable.postValue(Error(errorCode, errorMessage))
                    }
                },
                db,
                viewModelScope
            )
        }
    }

    fun navigateToCommentsScreen(bundle: Bundle) {
        observables.commentsFragmentLaunchObservable.value = bundle
    }

    companion object {
        private const val TAG = "MainViewModel"
    }
}
