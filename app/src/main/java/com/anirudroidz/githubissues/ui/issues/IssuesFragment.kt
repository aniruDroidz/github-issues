package com.anirudroidz.githubissues.ui.issues

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.anirudroidz.githubissues.MainViewModel
import com.anirudroidz.githubissues.data.model.issue.Issue
import com.anirudroidz.githubissues.databinding.MainFragmentBinding
import com.anirudroidz.githubissues.ui.issues.adapter.IssueListAdapter

class IssuesFragment : Fragment() {

    private lateinit var adapter: IssueListAdapter
    private lateinit var binding: MainFragmentBinding
    private lateinit var viewModel: MainViewModel
    private val issueList = mutableListOf<Issue>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = MainFragmentBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        activity?.let {
            viewModel = ViewModelProvider(it).get(MainViewModel::class.java)
            binding.lifecycleOwner = this

            initialiseViews()
            setupObservables()

            viewModel.initialiseMainFragment()
        }
    }

    private fun initialiseViews() {
        activity?.let {
            adapter = IssueListAdapter(it, issueList)
            binding.issueRv.layoutManager = LinearLayoutManager(activity)
            binding.issueRv.adapter = adapter
        }
    }

    private fun setupObservables() {
        viewModel.observables.errorObservable.observe(viewLifecycleOwner, {
            Log.e(TAG, "$it.errorCode, $it.errorMessage")
        })

        viewModel.observables.issuesObservable.observe(viewLifecycleOwner, {
            issueList.clear()
            issueList.addAll(it)
            adapter.notifyDataSetChanged()
        })
    }

    companion object {
        fun newInstance() = IssuesFragment()
        private const val TAG = "MainFragment"
    }
}
