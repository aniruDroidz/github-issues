package com.anirudroidz.githubissues.ui.comments.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.anirudroidz.githubissues.R
import com.anirudroidz.githubissues.data.model.comment.Comment
import com.anirudroidz.githubissues.databinding.CommentRowItemBinding

class CommentsAdapter(
    private val comments: List<Comment>
) :
    RecyclerView.Adapter<CommentsAdapter.ViewHolder>() {

    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(viewGroup.context)
                .inflate(R.layout.comment_row_item, viewGroup, false)
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val comment = comments[position]
        holder.binding.comment = comment
    }

    override fun getItemCount() = comments.size

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val binding = CommentRowItemBinding.bind(itemView)
    }
}