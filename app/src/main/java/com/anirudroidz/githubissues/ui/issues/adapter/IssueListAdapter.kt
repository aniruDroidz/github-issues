package com.anirudroidz.githubissues.ui.issues.adapter

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelStoreOwner
import androidx.recyclerview.widget.RecyclerView
import com.anirudroidz.githubissues.MainConstants
import com.anirudroidz.githubissues.MainViewModel
import com.anirudroidz.githubissues.R
import com.anirudroidz.githubissues.data.model.issue.Issue
import com.anirudroidz.githubissues.databinding.IssueRowItemBinding
import com.squareup.picasso.Picasso

class IssueListAdapter(
    viewModelStoreOwner: ViewModelStoreOwner,
    private val issues: List<Issue>
) :
    RecyclerView.Adapter<IssueListAdapter.ViewHolder>() {

    private var viewModel = ViewModelProvider(viewModelStoreOwner).get(MainViewModel::class.java)

    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(viewGroup.context)
                .inflate(R.layout.issue_row_item, viewGroup, false)
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val issue = issues[position]
        holder.binding.issue = issue

        issue.user?.avatarUrl?.let {
            Picasso.get()
                .load(it)
                .resize(128, 128)
                .into(holder.binding.profileIcon)
        }

        holder.itemView.setOnClickListener { _ ->
            issue.commentsUrl?.let {
                val bundle = Bundle()
                bundle.putString(MainConstants.COMMENTS_URL, it)
                bundle.putString(MainConstants.USERNAME_KEY, issue.user?.login)
                bundle.putString(MainConstants.PROFILE_ICON_KEY, issue.user?.avatarUrl)
                bundle.putString(MainConstants.TITLE_KEY, issue.title)
                bundle.putString(MainConstants.DESCRIPTION_KEY, issue.body)
                viewModel.navigateToCommentsScreen(bundle)
            }
        }
    }

    override fun getItemCount() = issues.size

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val binding = IssueRowItemBinding.bind(itemView)
    }
}