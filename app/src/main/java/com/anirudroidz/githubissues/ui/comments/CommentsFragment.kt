package com.anirudroidz.githubissues.ui.comments

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.anirudroidz.githubissues.MainConstants
import com.anirudroidz.githubissues.MainViewModel
import com.anirudroidz.githubissues.data.model.comment.Comment
import com.anirudroidz.githubissues.databinding.CommentsFragmentBinding
import com.anirudroidz.githubissues.ui.comments.adapter.CommentsAdapter
import com.squareup.picasso.Picasso

class CommentsFragment : Fragment() {

    private lateinit var adapter: CommentsAdapter
    private lateinit var binding: CommentsFragmentBinding
    private lateinit var viewModel: MainViewModel
    private val commentsList = mutableListOf<Comment>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = CommentsFragmentBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        activity?.let {
            viewModel = ViewModelProvider(it).get(MainViewModel::class.java)
            binding.lifecycleOwner = this

            initialiseViews()
            setupObservables()
            viewModel.initialiseCommentsFragment(
                arguments?.getString(
                    MainConstants.COMMENTS_URL,
                    MainConstants.EMPTY_STRING
                ) ?: MainConstants.EMPTY_STRING
            )
        }
    }

    private fun initialiseViews() {
        val username = arguments?.getString(
            MainConstants.USERNAME_KEY,
            MainConstants.EMPTY_STRING
        ) ?: MainConstants.EMPTY_STRING
        binding.userName.text = username

        val title = arguments?.getString(
            MainConstants.TITLE_KEY,
            MainConstants.EMPTY_STRING
        ) ?: MainConstants.EMPTY_STRING
        binding.title.text = title

        val description = arguments?.getString(
            MainConstants.DESCRIPTION_KEY,
            MainConstants.EMPTY_STRING
        ) ?: MainConstants.EMPTY_STRING
        binding.description.text = description

        val avatarUrl = arguments?.getString(MainConstants.PROFILE_ICON_KEY)

        avatarUrl?.let {
            Picasso.get()
                .load(avatarUrl)
                .resize(128, 128)
                .into(binding.profileIcon)
        }

        adapter = CommentsAdapter(commentsList)
        binding.commentsRv.layoutManager = LinearLayoutManager(activity)
        binding.commentsRv.adapter = adapter
    }

    private fun setupObservables() {
        viewModel.observables.errorObservable.observe(viewLifecycleOwner, {
            Log.e(TAG, "${it.errorCode}, ${it.errorMessage}")
        })

        viewModel.observables.commentsObservable.observe(viewLifecycleOwner, {
            commentsList.clear()
            commentsList.addAll(it)
            adapter.notifyDataSetChanged()
        })
    }

    companion object {
        fun newInstance(bundle: Bundle): CommentsFragment {
            val commentsFragment = CommentsFragment()
            commentsFragment.arguments = bundle
            return commentsFragment
        }

        private const val TAG = "MainFragment"
    }
}
