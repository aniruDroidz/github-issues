package com.anirudroidz.githubissues

import android.os.Bundle
import androidx.lifecycle.MutableLiveData
import com.anirudroidz.githubissues.data.model.Error
import com.anirudroidz.githubissues.data.model.comment.Comment
import com.anirudroidz.githubissues.data.model.issue.Issue

class MainObservables {
    val errorObservable = MutableLiveData<Error>()
    val issuesObservable = MutableLiveData<List<Issue>>()
    val commentsObservable = MutableLiveData<List<Comment>>()
    val commentsFragmentLaunchObservable = MutableLiveData<Bundle>()
}