package com.anirudroidz.githubissues

object MainConstants {
    const val DESCRIPTION_KEY = "description"
    const val TITLE_KEY = "title"
    const val USERNAME_KEY = "username"
    const val COMMENTS_URL = "comments_url"
    const val PROFILE_ICON_KEY = "profile_icon"
    const val EMPTY_STRING = ""
}