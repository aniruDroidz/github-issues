package com.anirudroidz.githubissues.data.model

data class Error(val errorCode: String, val errorMessage: String)