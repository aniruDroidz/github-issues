package com.anirudroidz.githubissues.data.model.issue

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity
class Issue {
    @PrimaryKey
    var id: Int? = null
    @ColumnInfo(name = "url")
    var url: String? = null
    @ColumnInfo(name = "repository_url")
    @SerializedName(value = "repository_url")
    var repositoryUrl: String? = null
    @ColumnInfo(name = "labels_url")
    @SerializedName(value = "labels_url")
    var labelsUrl: String? = null
    @ColumnInfo(name = "comments_url")
    @SerializedName(value = "comments_url")
    var commentsUrl: String? = null
    @ColumnInfo(name = "events_url")
    @SerializedName(value = "events_url")
    var eventsUrl: String? = null
    @ColumnInfo(name = "html_url")
    @SerializedName(value = "html_url")
    var htmlUrl: String? = null
    @ColumnInfo(name = "node_id")
    @SerializedName(value = "node_id")
    var nodeId: String? = null
    @ColumnInfo(name = "number")
    var number: Int? = null
    @ColumnInfo(name = "title")
    var title: String? = null
    @ColumnInfo(name = "state")
    var state: String? = null
    @ColumnInfo(name = "locked")
    var locked: Boolean? = null
    @ColumnInfo(name = "comments")
    var comments: Int? = null
    @ColumnInfo(name = "created_at")
    @SerializedName(value = "created_at")
    var createdAt: String? = null
    @ColumnInfo(name = "updated_at")
    @SerializedName(value = "updated_at")
    var updatedAt: String? = null
    @ColumnInfo(name = "author_association")
    @SerializedName(value = "author_association")
    var authorAssociation: String? = null
    @ColumnInfo(name = "body")
    var body: String? = null
    @ColumnInfo(name = "user")
    var user: User? = null

    override fun toString(): String {
        return "Issue(url=$url, repositoryUrl=$repositoryUrl, labelsUrl=$labelsUrl, commentsUrl=$commentsUrl, eventsUrl=$eventsUrl, htmlUrl=$htmlUrl, id=$id, nodeId=$nodeId, number=$number, title=$title, state=$state, locked=$locked, comments=$comments, createdAt=$createdAt, updatedAt=$updatedAt, authorAssociation=$authorAssociation, body=$body)"
    }
}