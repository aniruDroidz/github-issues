package com.anirudroidz.githubissues.data.model.issue

import androidx.room.ColumnInfo
import androidx.room.Entity

@Entity
class PullRequest {
    @ColumnInfo(name = "url")
    var url: String? = null
    @ColumnInfo(name = "html_url")
    var htmlUrl: String? = null
    @ColumnInfo(name = "diff_url")
    var diffUrl: String? = null
    @ColumnInfo(name = "patch_url")
    var patchUrl: String? = null

    override fun toString(): String {
        return "PullRequest(url=$url, htmlUrl=$htmlUrl, diffUrl=$diffUrl, patchUrl=$patchUrl)"
    }
}