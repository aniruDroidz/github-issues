package com.anirudroidz.githubissues.data.model.issue

import androidx.room.ColumnInfo
import androidx.room.Entity
import com.google.gson.annotations.SerializedName

@Entity
class User {
    @ColumnInfo(name = "login")
    var login: String? = null
    @ColumnInfo(name = "id")
    var id: Int? = null
    @ColumnInfo(name = "node_id")
    @SerializedName(value = "node_id")
    var nodeId: String? = null
    @ColumnInfo(name = "avatar_url")
    @SerializedName(value = "avatar_url")
    var avatarUrl: String? = null
    @ColumnInfo(name = "gravatar_id")
    @SerializedName(value = "gravatar_id")
    var gravatarId: String? = null
    @ColumnInfo(name = "url")
    var url: String? = null
    @ColumnInfo(name = "html_url")
    @SerializedName(value = "html_url")
    var htmlUrl: String? = null
    @ColumnInfo(name = "followers_url")
    @SerializedName(value = "followers_url")
    var followersUrl: String? = null
    @ColumnInfo(name = "following_url")
    @SerializedName(value = "following_url")
    var followingUrl: String? = null
    @ColumnInfo(name = "gists_url")
    @SerializedName(value = "gists_url")
    var gistsUrl: String? = null
    @ColumnInfo(name = "starred_rl")
    @SerializedName(value = "starred_rl")
    var starredUrl: String? = null
    @ColumnInfo(name = "subscriptions_url")
    @SerializedName(value = "subscriptions_url")
    var subscriptionsUrl: String? = null
    @ColumnInfo(name = "organizations_url")
    @SerializedName(value = "organizations_url")
    var organizationsUrl: String? = null
    @ColumnInfo(name = "repos_url")
    @SerializedName(value = "repos_url")
    var reposUrl: String? = null
    @ColumnInfo(name = "events_url")
    @SerializedName(value = "events_url")
    var eventsUrl: String? = null
    @ColumnInfo(name = "received_events_url")
    @SerializedName(value = "received_events_url")
    var receivedEventsUrl: String? = null
    @ColumnInfo(name = "type")
    var type: String? = null
    @ColumnInfo(name = "site_admin")
    @SerializedName(value = "site_admin")
    var siteAdmin: Boolean? = null

    override fun toString(): String {
        return "User(login=$login, id=$id, nodeId=$nodeId, avatarUrl=$avatarUrl, gravatarId=$gravatarId, url=$url, htmlUrl=$htmlUrl, followersUrl=$followersUrl, followingUrl=$followingUrl, gistsUrl=$gistsUrl, starredUrl=$starredUrl, subscriptionsUrl=$subscriptionsUrl, organizationsUrl=$organizationsUrl, reposUrl=$reposUrl, eventsUrl=$eventsUrl, receivedEventsUrl=$receivedEventsUrl, type=$type, siteAdmin=$siteAdmin)"
    }
}