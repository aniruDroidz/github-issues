package com.anirudroidz.githubissues.data.model.comment

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.anirudroidz.githubissues.data.model.issue.User

@Entity
class Comment {
    @ColumnInfo(name = "url")
    var url: String? = null
    @ColumnInfo(name = "html_url")
    var htmlUrl: String? = null
    @ColumnInfo(name = "issue_url")
    var issueUrl: String? = null
    @PrimaryKey
    var id: Int? = null
    @ColumnInfo(name = "node_id")
    var nodeId: String? = null
    @ColumnInfo(name = "created_at")
    var createdAt: String? = null
    @ColumnInfo(name = "updated_at")
    var updatedAt: String? = null
    @ColumnInfo(name = "author_association")
    var authorAssociation: String? = null
    @ColumnInfo(name = "body")
    var body: String? = null

    override fun toString(): String {
        return "Comment(url=$url, htmlUrl=$htmlUrl, issueUrl=$issueUrl, id=$id, nodeId=$nodeId, createdAt=$createdAt, updatedAt=$updatedAt, authorAssociation=$authorAssociation, body=$body)"
    }
}