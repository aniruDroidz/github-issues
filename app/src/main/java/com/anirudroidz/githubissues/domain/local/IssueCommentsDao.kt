package com.anirudroidz.githubissues.domain.local

import androidx.room.*
import com.anirudroidz.githubissues.data.model.comment.Comment

@Dao
interface IssueCommentsDao {
    @Query("SELECT * FROM comment")
    fun getAll(): List<Comment>

    @Query("SELECT * FROM issue WHERE id IS (:issueId)")
    fun loadAllByIds(issueId: Int): List<Comment>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll(issues: List<Comment>)

    @Delete
    fun delete(issue: Comment)
}