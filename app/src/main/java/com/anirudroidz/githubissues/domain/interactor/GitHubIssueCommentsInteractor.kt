package com.anirudroidz.githubissues.domain.interactor

import android.content.Context
import com.anirudroidz.githubissues.data.model.comment.Comment
import com.anirudroidz.githubissues.domain.Callback
import com.anirudroidz.githubissues.domain.IGitHubRepository
import com.anirudroidz.githubissues.domain.local.AppDatabase
import kotlinx.coroutines.CoroutineScope

class GitHubIssueCommentsInteractor(
    private val context: Context?,
    private val repository: IGitHubRepository
) {
    private val dispose = false

    suspend fun execute(issueId: String, callback: Callback<List<Comment>>, db: AppDatabase, scope: CoroutineScope) {
        if(!dispose) {
            repository.listIssueComments(issueId, context, db, scope, callback)
        }
    }
}