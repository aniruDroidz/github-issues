package com.anirudroidz.githubissues.domain.local

import androidx.room.*
import com.anirudroidz.githubissues.data.model.issue.Issue

@Dao
interface IssueDao {
    @Query("SELECT * FROM issue")
    fun getAll(): List<Issue>

    @Query("SELECT * FROM issue WHERE id IN (:id)")
    fun loadAllByIds(id: Int): List<Issue>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll(issues: List<Issue>)

    @Delete
    fun delete(issue: Issue)
}