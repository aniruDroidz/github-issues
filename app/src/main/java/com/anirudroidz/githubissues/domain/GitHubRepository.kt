package com.anirudroidz.githubissues.domain

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkInfo
import com.anirudroidz.githubissues.R
import com.anirudroidz.githubissues.data.model.comment.Comment
import com.anirudroidz.githubissues.data.model.issue.Issue
import com.anirudroidz.githubissues.domain.local.AppDatabase
import com.anirudroidz.githubissues.domain.remote.IGitHubService
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object GitHubRepository: IGitHubRepository {
    private val retrofit = Retrofit.Builder()
        .baseUrl("https://api.github.com/")
        .addConverterFactory(GsonConverterFactory.create())
        .build()

    override suspend fun listIssues(
        context: Context?,
        db: AppDatabase,
        scope: CoroutineScope,
        callBack: com.anirudroidz.githubissues.domain.Callback<List<Issue>>
    ) {
        if(context != null && isConnectedToNetwork(context)) {
            val githubService = retrofit.create(IGitHubService::class.java)
            githubService.listIssues().enqueue(object : Callback<List<Issue>> {
                override fun onResponse(call: Call<List<Issue>>, response: Response<List<Issue>>) {
                    response.body()?.let {
                        callBack.onResponse(it)
                        scope.launch {
                            db.issueDao().insertAll(it)
                        }
                    }
                }

                override fun onFailure(call: Call<List<Issue>>, t: Throwable) {
                    callBack.onError(t.message ?: context.getString(R.string.error),
                        t.localizedMessage ?: context.getString(R.string.something_went_wrong))
                }
            })
        } else {
            withContext(Dispatchers.IO) {
                callBack.onResponse(db.issueDao().getAll().reversed())
            }
        }
    }

    override suspend fun listIssueComments(
        issueId: String,
        context: Context?,
        db: AppDatabase,
        scope: CoroutineScope,
        callBack: com.anirudroidz.githubissues.domain.Callback<List<Comment>>
    ) {
        if(context != null && isConnectedToNetwork(context)) {
            val githubService = retrofit.create(IGitHubService::class.java)
            githubService.listIssueComments(issueId).enqueue(object : Callback<List<Comment>> {
                override fun onResponse(call: Call<List<Comment>>, response: Response<List<Comment>>) {
                    response.body()?.let {
                        callBack.onResponse(it)
                        scope.launch {
                            db.issueCommentsDao().insertAll(it)
                        }
                    }
                }

                override fun onFailure(call: Call<List<Comment>>, t: Throwable) {
                    callBack.onError(t.message ?: context.getString(R.string.error),
                        t.localizedMessage ?: context.getString(R.string.something_went_wrong))
                }
            })
        } else {
            withContext(Dispatchers.IO) {
                callBack.onResponse(db.issueCommentsDao().getAll().reversed())
            }
        }
    }

    private fun isConnectedToNetwork(context: Context): Boolean {
        val cm = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager?
        val activeNetwork: NetworkInfo? = cm?.activeNetworkInfo
        return activeNetwork?.isConnectedOrConnecting == true
    }
}