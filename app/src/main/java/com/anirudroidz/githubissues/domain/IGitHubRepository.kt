package com.anirudroidz.githubissues.domain

import android.content.Context
import com.anirudroidz.githubissues.data.model.comment.Comment
import com.anirudroidz.githubissues.data.model.issue.Issue
import com.anirudroidz.githubissues.domain.local.AppDatabase
import kotlinx.coroutines.CoroutineScope

interface IGitHubRepository {
    suspend fun listIssues(context: Context?, db: AppDatabase, scope: CoroutineScope, callBack: Callback<List<Issue>>)

    suspend fun listIssueComments(issueId: String, context: Context?, db: AppDatabase, scope: CoroutineScope, callBack: Callback<List<Comment>>)
}