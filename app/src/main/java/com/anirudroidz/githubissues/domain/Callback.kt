package com.anirudroidz.githubissues.domain

interface Callback<T> {

    fun onResponse(response: T)

    fun onError(errorCode: String, errorMessage: String)
}