package com.anirudroidz.githubissues.domain.interactor

import android.content.Context
import com.anirudroidz.githubissues.data.model.issue.Issue
import com.anirudroidz.githubissues.domain.Callback
import com.anirudroidz.githubissues.domain.IGitHubRepository
import com.anirudroidz.githubissues.domain.local.AppDatabase
import kotlinx.coroutines.CoroutineScope

class GitHubIssuesInteractor(
    private val context: Context?,
    private val repository: IGitHubRepository) {
    private val dispose = false

    suspend fun execute(callback: Callback<List<Issue>>, db: AppDatabase, scope: CoroutineScope) {
        if(!dispose) {
            repository.listIssues(context, db, scope, callback)
        }
    }
}