package com.anirudroidz.githubissues.domain.local

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverter
import androidx.room.TypeConverters
import com.anirudroidz.githubissues.data.model.comment.Comment
import com.anirudroidz.githubissues.data.model.issue.Issue
import com.anirudroidz.githubissues.data.model.issue.User
import com.google.gson.Gson

@Database(entities = [Issue::class, Comment::class], version = 1)
@TypeConverters(Converters::class)
abstract class AppDatabase : RoomDatabase() {
    abstract fun issueDao(): IssueDao

    abstract fun issueCommentsDao(): IssueCommentsDao
}

class Converters {
    @TypeConverter
    fun toUser(json: String): User {
        val gson = Gson()
        return gson.fromJson(json, User::class.java)
    }

    @TypeConverter
    fun fromUser(user: User): String {
        val gson = Gson()

        return gson.toJson(user)
    }
}