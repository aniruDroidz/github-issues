package com.anirudroidz.githubissues.domain.remote

import com.anirudroidz.githubissues.data.model.comment.Comment
import com.anirudroidz.githubissues.data.model.issue.Issue
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Url

interface IGitHubService {
    @GET("repos/square/okhttp/issues")
    fun listIssues(): Call<List<Issue>>

    @GET
    fun listIssueComments(@Url url: String): Call<List<Comment>>
}
