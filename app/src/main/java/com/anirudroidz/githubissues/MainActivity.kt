package com.anirudroidz.githubissues

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.room.Room
import com.anirudroidz.githubissues.domain.GitHubRepository
import com.anirudroidz.githubissues.domain.interactor.GitHubIssueCommentsInteractor
import com.anirudroidz.githubissues.domain.interactor.GitHubIssuesInteractor
import com.anirudroidz.githubissues.domain.local.AppDatabase
import com.anirudroidz.githubissues.ui.comments.CommentsFragment
import com.anirudroidz.githubissues.ui.issues.IssuesFragment

class MainActivity : AppCompatActivity() {

    private val issueFragment = IssuesFragment.newInstance()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main_activity)
        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction()
                .add(R.id.container, issueFragment, ISSUE_FRAGMENT_TAG)
                .commit()
        }

        val viewModel = ViewModelProvider(this, object : ViewModelProvider.Factory {
            override fun <T : ViewModel?> create(modelClass: Class<T>): T {
                val db = Room.databaseBuilder(
                    applicationContext,
                    AppDatabase::class.java, DATABASE_NAME
                ).build()
                return MainViewModel(
                    GitHubIssuesInteractor(this@MainActivity, GitHubRepository),
                    GitHubIssueCommentsInteractor(this@MainActivity, GitHubRepository),
                    db,
                    MainObservables()
                ) as T
            }
        }).get(MainViewModel::class.java)

        initiliseObservables(viewModel)
    }

    private fun initiliseObservables(viewModel: MainViewModel) {
        viewModel.observables.commentsFragmentLaunchObservable.observe(
            this, {
                supportFragmentManager.beginTransaction()
                    .replace(R.id.container, CommentsFragment.newInstance(it), COMMENTS_FRAGMENT_TAG)
                    .addToBackStack(CommentsFragment::class.simpleName)
                    .hide(issueFragment)
                    .commit()
            })
    }

    override fun onBackPressed() {
        if (supportFragmentManager.backStackEntryCount > 0) {
            supportFragmentManager.popBackStackImmediate()
            supportFragmentManager.beginTransaction().show(issueFragment)
        } else {
            super.onBackPressed();
        }
    }

    companion object {
        private const val ISSUE_FRAGMENT_TAG = "issue-fragment"
        private const val COMMENTS_FRAGMENT_TAG = "comments-fragment"
        private const val DATABASE_NAME = "github-issues"
    }
}