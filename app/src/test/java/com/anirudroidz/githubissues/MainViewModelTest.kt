package com.anirudroidz.githubissues

import android.os.Bundle
import androidx.lifecycle.MutableLiveData
import com.anirudroidz.githubissues.data.model.comment.Comment
import com.anirudroidz.githubissues.data.model.issue.Issue
import com.anirudroidz.githubissues.domain.interactor.GitHubIssueCommentsInteractor
import com.anirudroidz.githubissues.domain.interactor.GitHubIssuesInteractor
import com.anirudroidz.githubissues.domain.local.AppDatabase
import io.mockk.CapturingSlot
import io.mockk.MockKAnnotations
import io.mockk.coVerify
import io.mockk.impl.annotations.InjectMockKs
import io.mockk.impl.annotations.MockK
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Before
import org.junit.Test

class MainViewModelTest {

    @MockK
    private lateinit var gitHubIssuesInteractor: GitHubIssuesInteractor

    @MockK
    private lateinit var gitHubIssueCommentsInteractor: GitHubIssueCommentsInteractor

    @MockK
    private lateinit var appDatabase: AppDatabase

    @MockK
    private lateinit var mainObservables: MainObservables

    @MockK
    private lateinit var bundle: Bundle

    @MockK
    private lateinit var commentsFragmentLaunchObservable: MutableLiveData<Bundle>

    private val issuesCallback =
        CapturingSlot<com.anirudroidz.githubissues.domain.Callback<List<Issue>>>()

    private val commentsCallback =
        CapturingSlot<com.anirudroidz.githubissues.domain.Callback<List<Comment>>>()

    @InjectMockKs
    private lateinit var mainViewModel: MainViewModel

    @ExperimentalCoroutinesApi
    val dispatcher = TestCoroutineDispatcher()

    @ExperimentalCoroutinesApi
    @Before
    fun setup() {
        MockKAnnotations.init(this, relaxUnitFun = true)

        Dispatchers.setMain(dispatcher)
    }

    @Test
    fun initialiseMainFragment() {
        mainViewModel.initialiseMainFragment()

        coVerify {
            gitHubIssuesInteractor.execute(
                capture(issuesCallback),
                appDatabase,
                any()
            )
        }
    }

    @Test
    fun initialiseCommentsFragment() {
        mainViewModel.initialiseCommentsFragment(TEST_URL)

        coVerify {
            gitHubIssueCommentsInteractor.execute(
                TEST_URL,
                capture(commentsCallback),
                appDatabase,
                any()
            )
        }
    }

    @ExperimentalCoroutinesApi
    @After
    fun tearDown() {
        Dispatchers.resetMain() // reset main dispatcher to the original Main dispatcher
    }

    companion object {
        private const val TEST_URL = "TEST_URL"
    }
}